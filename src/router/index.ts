import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'main',
    component: () => import('@/pages/MainPage/MainPage.vue'),
    children: [
      {
        path: '/',
        name: 'lines',
        component: () => import('../pages/LinesPage/LinesPage.vue'),
      },
      {
        path: '/stops',
        name: 'stops',
        component: () => import('../pages/StopsPage/StopsPage.vue'),
      },
    ]
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
