import { createStore, Store } from "vuex";
import { debounce } from "@/utils/debounce";

interface DataType {
  line: number;
  stop: string;
  order: number;
  time: string;
}

type LinesMap = Record<string, number[]>;

interface StateType {
  data: DataType[];
  linesMap: LinesMap;
  selectedLineNumber: number | null;
  selectedLineStation: string;
  busStopsFilter: string;
}

const store = createStore<StateType>({
  state: {
    data: [],
    linesMap: {},
    selectedLineNumber: null,
    selectedLineStation: "",
    busStopsFilter: "",
  },
  getters: {
    linesNumbers(state) {
      return Object.keys(state.linesMap);
    },
    selectedLineStations({ linesMap, selectedLineNumber, data }) {
      if (!selectedLineNumber) return [];

      const keyMap = new Map();
      for (const index of linesMap[selectedLineNumber]) {
        keyMap.set(data[index].stop, data[index].order);
      }
      return Array.from(keyMap.entries())
        .sort(([, a], [, b]) => a - b)
        .map(([name]) => name as string);
    },
    selectedStationTimetable({
      selectedLineStation,
      selectedLineNumber,
      linesMap,
      data,
    }) {
      if (!selectedLineStation || !selectedLineNumber) return [];

      const result: string[][] = [];
      for (const i of linesMap[selectedLineNumber]) {
        if (data[i].stop === selectedLineStation)
          result.push(data[i].time.split(":"));
      }
      return result
        .sort((a, b) => {
          if (+a[0] * 100 + +a[1] < +b[0] * 100 + +b[1]) return -1;
          return 1;
        })
        .map((hm) => hm.join(":"));
    },
    filteredStations(state) {
      const result = new Set<string>();
      for (const el of state.data) {
        if (
          el.stop.toLowerCase().includes(state.busStopsFilter.toLowerCase())
        ) {
          result.add(el.stop);
        }
      }
      return Array.from(result).sort((a, b) => b.localeCompare(a));
    },
  },
  mutations: {
    setBusStopFilter(state, payload) {
      state.busStopsFilter = payload;
    },
    setLineNumber(state, payload) {
      state.selectedLineNumber = payload;
      state.selectedLineStation = "";
    },
    setStation(state, payload) {
      state.selectedLineStation = payload;
    },
    setInitialData(state, payload: DataType[]) {
      const linesMap: LinesMap = {};
      for (let i = 0; i < payload.length; i++) {
        const el = payload[i];
        if (!(el.line in linesMap)) linesMap[el.line] = [];
        linesMap[el.line].push(i);
      }
      state.data = payload;
      state.linesMap = linesMap;
    },
  },
  actions: {
    debouncedSetBusStopFilter: debounce(({ commit }, payload: string) => {
      commit("setBusStopFilter", payload);
    }, 300),
    async loadData({ commit }) {
      const res = await fetch("http://localhost:3000/stops", {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      });
      const data = await res.json();
      commit("setInitialData", data);
    },
  },
});

declare module "@vue/runtime-core" {
  interface CustomProperties {
    $store: Store<StateType>;
  }
}

export default store;
